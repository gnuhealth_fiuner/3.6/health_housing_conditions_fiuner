# -*- coding: utf-8 -*-
##############################################################################

from trytond.pool import Pool
from .health_housing import *
from .configuration import *
from .health import *
from .address import *
from .wizard import *

def register():
    Pool.register(
        HousingConditions,
        DomiciliaryUnit,
        Party,
        Address,    
        Configuration,
        ConfigurationDefault,
        GnuHealthSequences,
        DUDetected,
        WizardUpdateDUStart,
        WizardUpdateDUList,
        module='health_housing_conditions_fiuner', type_='model')
    Pool.register(
        WizardUpdateDU,
        module='health_housing_conditions_fiuner', type_='wizard')
