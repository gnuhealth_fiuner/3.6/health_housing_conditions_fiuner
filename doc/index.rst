GNU Health DU (fiuner) module
#######################################

This module add support to:
     * Add a configuration menu to :
            Set up default city, zip code, subdivision, country, fields on party_address
            and DU registers
            Set up default citizenship and residence fields on party_party registers
     * Enable bool field to automplete "code" field on DU
     * Enable to register housing conditions by date, according to health census required by
     Ministry of Health of Entre Rios
     * 
     * 

